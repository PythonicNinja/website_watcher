# -*- coding: utf-8 -*-
# CREATED ON DATE: 31.01.15
__author__ = 'vojtek.nowak@gmail.com'

import time
import base64
import urllib2
import smtplib


def log_to_gmail(user, password):

    gmail_user = user
    gmail_pwd = password
    smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(gmail_user, gmail_pwd)

    return smtpserver


def watch(url, callback_function, callback_args, gmail, basic_auth=None, interval=1):
    old_length = 0
    while True:
        old_length = get_website(url=url,
                                 basic_auth=basic_auth,
                                 gmail=gmail,
                                 old_length=old_length,
                                 callback_function=callback_function,
                                 callback_args=callback_args)
        time.sleep(interval)


def send_mail(url, gmail, send_to, initial=True):
    sender = 'watchscript@python.com'

    if initial:
        message = """

        %(title)s

        Thanks for using website watcher.
        """ % ({
            'from': sender,
            'to': send_to,
            'title': str(url) + ':\t' + 'is now watched by script',
            'url': url
        })
    else:
        message = """

        %(title)s

        Thanks for using website watcher.
        """ % ({
            'from': sender,
            'to': send_to,
            'title': str(url) + ':\t' + 'has been changed',
            'url': url
        })
    receivers = [send_to]
    try:
       smtpObj = log_to_gmail(**gmail)
       smtpObj.sendmail(sender, receivers, message)
       print "Successfully sent email"
    except smtplib.SMTPException:
       print "Error: unable to send email"


def get_website(url, basic_auth, gmail, old_length, callback_function, callback_args):

    request = urllib2.Request(url)
    if basic_auth:
        base64string = base64.encodestring('%s:%s' % (basic_auth.get('user'), basic_auth.get('password'))).replace('\n', '')
        request.add_header("Authorization", "Basic %s" % base64string)
    request.get_method = lambda: 'GET'

    response = urllib2.urlopen(request)
    NEW_LENGTH = len(response.read())

    if old_length != NEW_LENGTH:
        print "something has changed"
        callback_function(url=url, gmail=gmail, send_to=callback_args, initial=(old_length == 0))

    return NEW_LENGTH


if __name__ == '__main__':
    watch(url='https://inf.ug.edu.pl/~wpawlowski/lab/LDI1/',
          callback_function=send_mail,
          basic_auth={
              'user': 'ldi1',
              'password': 'UG2@15'
          },
          gmail={
              'user': 'watchScriptwatchScript',
              'password': 'watchScriptwatchScriptwatchScript'
          },
          callback_args='vojtek.nowak@gmail.com')
