# README #

### What is this repository for? ###

This script is intended to watch website for any changes and notify you by email. Even if website require basic auth.

![Zrzut ekranu 2015-01-31 o 22.25.05.png](https://bitbucket.org/repo/Kenq68/images/2046219441-Zrzut%20ekranu%202015-01-31%20o%2022.25.05.png)


### How do I get set up? ###

* Install [python](https://www.python.org/ftp/python/2.7.9/python-2.7.9-macosx10.6.pkg)
* Clone repo
* Edit your configuration acordingly

```
#!python

if __name__ == '__main__':
    watch(url='https://inf.ug.edu.pl/~wpawlowski/lab/LDI1/',
          callback_function=send_mail,
          basic_auth={
              'user': 'ldi1',
              'password': 'UG2@15'
          },
          gmail={
              'user': 'watchScriptwatchScript',
              'password': 'watchScriptwatchScriptwatchScript'
          },
          callback_args='vojtek.nowak@gmail.com')
```

* python watcher.py

#### I recommend runing this script under screen on unix machines.

### Who do I talk to? ###

* vojtek.nowak@gmail.com - [zBlo.ga](http://zblo.ga/)